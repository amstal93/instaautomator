# InstaAutomator
Project is targeted to attract more users on instagram through posts on different niches.

# Implementation Requirements
| Name | Requirement |
| ------ | ------ |
| AWS | Hosting and platform |
| Lambdas | Functional code to post on instagram |
| git lab | versioning |
| gitlab cicd | Deployment |
| AWS CLI | To configure .aws credentials |
| Docker | To build custom docker image |
| Terraform | Build and deploy aws resources (especially lambda functions, iam roles) |

# Steps involved
1. Install and configure docker, terraform, aws cli
2. Create IAM user for terraform code
3. Pull python3.7 base image and install requirements in docker
4. Push the custom built docker image to docker hub
5. Create terraform code to provision AWS resources
6. Write build script in python for lambda functions
7. Build gitlab ci file and push to git repo
8. Check the errors in the gitlab pipeline
