# Create the function
resource "aws_lambda_function" "Automator-Lambda" {
  filename         = "../bin/lambda_automator.zip"
  function_name    = "Automator-Lambda"
  role             = aws_iam_role.iam_for_lambda_tf.arn
  handler          = "code.lambda_handler"
  source_code_hash = filebase64sha256("../bin/lambda_automator.zip")
  runtime          = "python3.7"
  layers           = [aws_lambda_layer_version.automator-lambda-layer.arn]
}

# Necessary permissions to create/run the function
resource "aws_iam_role" "iam_for_lambda_tf" {
  name = "iam_for_lambda_tf"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}